var express = require('express');
var router = express.Router();
var mongojs = require('mongojs');
var db = mongojs('mongodb://admin:admin123@ds037827.mongolab.com:37827/ng2ideaapp', ['ideas']);
 
/* GET All ideas */
router.get('/ideas', function(req, res, next) {
    db.ideas.find(function(err, ideas) {
        if (err) {
            res.send(err);
        } else {
            res.json(ideas);
        }
    });
});
 
/* GET One Idea with the provided ID */
router.get('/idea/:id', function(req, res, next) {
    db.ideas.findOne({
        _id: mongojs.ObjectId(req.params.id)
    }, function(err, ideas) {
        if (err) {
            res.send(err);
        } else {
            res.json(ideas);
        }
    });
});
 
/* POST/SAVE a Idea */
router.post('/idea', function(req, res, next) {
    var idea = req.body;
    if (!idea.text || !(idea.isCompleted + '')) {
        res.status(400);
        res.json({
            "error": "Invalid Data"
        });
    } else {
        db.ideas.save(idea, function(err, result) {
            if (err) {
                res.send(err);
            } else {
                res.json(result);
            }
        })
    }
});
 
/* PUT/UPDATE a Idea */
router.put('/idea/:id', function(req, res, next) {
    var idea = req.body;
    var updObj = {};
 
    if (idea.isCompleted) {
        updObj.isCompleted = idea.isCompleted;
    }
    if (idea.text) {
        updObj.text = idea.text;
    }
 
    if (!updObj) {
        res.status(400);
        res.json({
            "error": "Invalid Data"
        });
    } else {
        db.ideas.update({
            _id: mongojs.ObjectId(req.params.id)
        }, updObj, {}, function(err, result) {
            if (err) {
                res.send(err);
            } else {
                res.json(result);
            }
        });
    }
 
 
});
 
/* DELETE a Idea */
router.delete('/idea/:id', function(req, res) {
    db.ideas.remove({
        _id: mongojs.ObjectId(req.params.id)
    }, '', function(err, result) {
        if (err) {
            res.send(err);
        } else {
            res.json(result);
        }
    });
 
});
 
module.exports = router;